import weechat

def open_log_file():
    """
    Open the log file where the raw IRC messages will be saved.

    The log file is created in the default weechat directory, with the name 'global_raw.log'.
    If the file already exists, the new messages will be appended to it.

    Returns:
        file: The opened log file.
    """
    filename = weechat.string_eval_path_home("${weechat_data_dir}/logs/global_raw.log",{},{},{})
    log_file = open(filename, "a")
    return log_file

def capture_raw_message(data, signal, signal_data):
    """
    Callback function for Weechat's Signal Hook, adds raw IRC message to a previously opened logfile.

    log_file needs to be declared before this function is called.
    """ 
    log_file.write(f"{signal_data}\r\n")
    log_file.flush()
    return weechat.WEECHAT_RC_OK



weechat.register("raw_message_capture", "snowpoke", "1.0", "GPL3", "Capture and record raw IRC messages", "", "")

log_file = open_log_file()
# https://weechat.org/files/doc/weechat/stable/weechat_scripting.en.html#irc_catch_messages
weechat.hook_signal("*,irc_raw_in2_*", "capture_raw_message", "")
