# Weechat IRC Raw Message Logger

This is a Python script for Weechat, a popular extensible chat client. It captures and records raw IRC messages for later analysis or debugging. 

## Features

The features of this script are as basic as it gets.

- Captures raw IRC messages in real-time.
- Stores the messages in a log file located in the default Weechat directory (data folder).
- Appends new messages to the log file, preserving historical data.

## Usage

1. Save the script `rawlogger.py` in your Weechat Python plugins directory.
2. In Weechat, type `/python load rawlogger.py` to load the plugin.
3. The plugin will immediately start logging raw IRC messages to the `global_raw.log` file in the Weechat directory.

## License

This project is licensed under GPL3.